/*Skrevet av Magnus Rambech, Thomas Rognes, Tord Kvife*/ 


/* åpner resultatsiden når man trykker på søk */
function loadSearchResults(){
	window.open("search_results.html", "_self")
}


/* Gir tilbakemelding på feedbacken */
function feedback(){
	
	if(document.getElementById('rating_feedback').innerHTML=="Takk for din tilbakemelding!"){
		window.alert("Du har allerede gitt en tilbakemelding!")
	}
	else{
		document.getElementById('rating_feedback').innerHTML="Takk for din tilbakemelding!"
	}
}

/* Innloggingsfunksjon */
function login(){
	var username = document.getElementById('username').value;
	var password = document.getElementById('password').value;
	if(username=="admin" && password == "admin"){
		window.alert("Du er nå logget inn!");
		window.open("index.html", "_self");
	}
	else{
		window.alert("FEIL INNLOGGING");
	}
}

/* Åpner innloggingssiden når man trykker på "logg inn" - knappen */
function loadLoginPage(){
	window.open("login.html", "_self");
}

/*Bytter bilde i slideshow*/
function byttBilde(n){
	document.getElementById("main").src = n;
}

/* vis/ skjul sammendrag */
function hideShow(){
	var x = document.getElementById("summary_movie");
	if (x.style.display === 'none') {
		x.style.display = 'block';
		document.getElementById('summary_button').innerHTML = "Skjul sammendrag";
	} else{
		document.getElementById('summary_button').innerHTML = "Vis sammendrag";
		x.style.display = 'none';
	}
}